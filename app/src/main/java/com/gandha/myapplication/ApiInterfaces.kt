package com.gandha.myapplication

import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterfaces {
    @POST("/mcash/user_info")
    fun user_info(@Header("X-Perusahaan-Id") header: String?): Call<UserInfoResponse?>?

    interface user_infoCallback {
        fun onSuccess(value: UserInfoResponse)
        fun onFailed(value: String)
        fun onError(throwable: Throwable)
    }
}