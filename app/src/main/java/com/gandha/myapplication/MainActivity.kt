package com.gandha.myapplication

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUserInfo()
    }

    private fun initUserInfo() {
        ApiUtils.user_info("TELKOMSEL-081319971046", object : ApiInterfaces.user_infoCallback {
            override fun onSuccess(value: UserInfoResponse) {
                Toast.makeText(applicationContext, value.message, Toast.LENGTH_LONG).show()
            }

            override fun onFailed(value: String) {
                Toast.makeText(applicationContext, value, Toast.LENGTH_LONG).show()
            }

            override fun onError(throwable: Throwable) {
                Toast.makeText(applicationContext, throwable.localizedMessage, Toast.LENGTH_LONG)
                    .show()
            }
        })
    }
}
