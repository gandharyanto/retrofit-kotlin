package com.gandha.myapplication

class UserInfoResponse {
    var success = false
    var message: String? = null
    var response: List<Response>? =
        null

    inner class Response {
        var rs_id: String? = null
        var description: String? = null
        var contact: String? = null
        var email: String? = null
        var deposit: String? = null
        var ar: String? = null
        var status: String? = null
        var price_id: String? = null
        var balance: String? = null
        var username: String? = null
        var level: String? = null
        var agent_expire: String? = null
        var chance: String? = null
        var image_file: String? = null
        var has_phone: String? = null
        var bca_va: String? = null
        var mandiri_scm: String? = null
        var cashback: String? = null
        var kodeunik: String? = null
        var permata_va: String? = null
        var mandiri_va: String? = null
        var chgpwd: String? = null
        var niaga_va: String? = null

    }
}