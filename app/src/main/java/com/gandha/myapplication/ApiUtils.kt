package com.gandha.myapplication

import com.gandha.myapplication.ApiInterfaces.user_infoCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object ApiUtils {
    const val BASE_URL = "https://apiv2.mdd.co.id:30308/"
    val aPIService: ApiInterfaces
        get() = RetrofitClient.getClient(BASE_URL)
            .create(ApiInterfaces::class.java)

    fun user_info(header: String?, mCallback: user_infoCallback) {
        val mApiService = aPIService
        mApiService.user_info(header)!!.enqueue(object : Callback<UserInfoResponse?> {
            override fun onResponse(
                call: Call<UserInfoResponse?>,
                response: Response<UserInfoResponse?>
            ) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body()!!)
                } else {
                    mCallback.onFailed(response.raw().message())
                }
            }

            override fun onFailure(
                call: Call<UserInfoResponse?>,
                t: Throwable
            ) {
                mCallback.onError(t)
            }
        })
    }
}